#!/usr/bin/env bash

# Export path for SSL Certificates
PSH_SSL_EXPORT_PATH=/app/.acme.sh/export
if [ ! -d "$PSH_SSL_EXPORT_PATH" ]; then
    mkdir -p ${PSH_SSL_EXPORT_PATH}
fi

# Get Platform.sh domains
PSH_DOMAINS=$(platform domain:list --columns=name --format tsv --no-header)
DOMAINS_PARAM=''
for PSH_DOMAIN in $PSH_DOMAINS
do
    DOMAINS_PARAM="${DOMAINS_PARAM} --domain ${PSH_DOMAIN}"
done

# Issue and export SSL certificates
./acme.sh \
    --issue \
    --force \
    ${DOMAINS_PARAM} \
    --webroot /app/web \
    --cert-file ${PSH_SSL_EXPORT_PATH}/cert.pem \
    --key-file ${PSH_SSL_EXPORT_PATH}/key.pem \
    --fullchain-file ${PSH_SSL_EXPORT_PATH}/chain.pem